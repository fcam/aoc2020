# AOC 2020

## Run docker 
```bash
docker run -d \
    -p 8888:8888 \
    -v /path/to/localRepo/notebooks/:/home/clojure/notebooks \
    kxxoling/jupyter-clojure-docker
```

then docker logs to get the login token
